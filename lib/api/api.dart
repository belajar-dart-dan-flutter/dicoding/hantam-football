import 'package:http/http.dart' as http;

class CallApi {
  final String _url = 'https://www.thesportsdb.com/api/v1/json/1/';

  getData(apiUrl) async {
    var fullUrl = _url + apiUrl;
    return await http.get(fullUrl, headers: _setHeaders());
  }

  _setHeaders() => {
        'Content-type': 'application/json',
        'Accept': 'application/json',
      };
}
