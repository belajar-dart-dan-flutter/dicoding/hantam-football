import 'package:flutter/material.dart';
import 'package:hantamFootball/api/api.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:hantamFootball/pages/DetailTeamScreen.dart';
import 'dart:convert';

// import model
import 'package:hantamFootball/model/Team.dart';

class AllTeamScreen extends StatefulWidget {
  @override
  _AllTeamScreenState createState() => _AllTeamScreenState();
}

class _AllTeamScreenState extends State<AllTeamScreen> {
  List<Team> _teams = List<Team>();
  bool _isLooading = true;

  @override
  void initState() {
    super.initState();
    _getTeams();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Daftar Team'),
      ),
      body: SafeArea(
        child: (() {
          if (_isLooading) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return SingleChildScrollView(
              padding: EdgeInsets.all(10),
              child: ResponsiveGridRow(
                children: _teams.map((team) {
                  return ResponsiveGridCol(
                    xs: 6,
                    sm: 4,
                    lg: 3,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(
                          builder: (context) {
                            return DetailTeamScreen(team: team);
                          },
                        ));
                      },
                      child: Hero(
                        tag: team.idTeam,
                        child: Card(
                          margin: EdgeInsets.all(10),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          elevation: 2,
                          child: Container(
                            padding: EdgeInsets.all(12),
                            child: Stack(
                              children: [
                                Center(
                                  child: Column(
                                    children: [
                                      Image.network(team.strTeamBadge),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(
                                        team.strTeam,
                                        style: TextStyle(fontSize: 15),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  );
                }).toList(),
              ),
            );
          }
        }()),
      ),
    );
  }

  @override
  void _getTeams() {
    CallApi().getData('lookup_all_teams.php?id=4328').then((response) {
      final result = json.decode(response.body);
      Iterable teams = result['teams'];
      setState(() {
        _isLooading = false;
        _teams = teams.map((team) => Team.fromJson(team)).toList();
      });
    });
  }
}
