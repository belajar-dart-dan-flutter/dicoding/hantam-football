import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class DetailTeamScreen extends StatefulWidget {
  final team;
  DetailTeamScreen({@required this.team});

  @override
  _DetailTeamScreenState createState() => _DetailTeamScreenState();
}

class _DetailTeamScreenState extends State<DetailTeamScreen> {
  final double expandedHeight = 200;
  final double roundedContainerHeight = 50;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          CustomScrollView(
            slivers: <Widget>[
              buildSilverHead(),
              SliverToBoxAdapter(
                child: buildDetail(),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget buildSilverHead() {
    return SliverPersistentHeader(
      delegate: DetailSilverDelegate(
        expandedHeight,
        roundedContainerHeight,
        widget.team,
      ),
    );
  }

  Widget buildDetail() {
    return Container(
      color: Colors.white,
      child: Column(
        children: <Widget>[
          buildTeamInfo(),
          Container(
            height: 150,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                Card(
                  child: Padding(
                    padding: EdgeInsets.all(12),
                    child: Image.network(widget.team.strTeamJersey),
                  ),
                ),
                Card(
                  child: Padding(
                    padding: EdgeInsets.all(12),
                    child: Image.network(widget.team.strTeamFanart1),
                  ),
                ),
                Card(
                  child: Padding(
                    padding: EdgeInsets.all(12),
                    child: Image.network(widget.team.strTeamFanart2),
                  ),
                ),
                Card(
                  child: Padding(
                    padding: EdgeInsets.all(12),
                    child: Image.network(widget.team.strTeamFanart3),
                  ),
                ),
                Card(
                  child: Padding(
                    padding: EdgeInsets.all(12),
                    child: Image.network(widget.team.strTeamFanart4),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
              vertical: 15,
              horizontal: 15,
            ),
            child: Text(
              widget.team.strDescriptionEn,
              style: TextStyle(
                color: Colors.black54,
                height: 1.4,
                fontSize: 15,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildTeamInfo() {
    return ListTile(
      leading: Image.network(widget.team.strTeamBadge),
      title: Text(widget.team.strTeam),
      subtitle: Text(widget.team.strAlternate),
    );
  }
}

class DetailSilverDelegate extends SliverPersistentHeaderDelegate {
  final double expandedHeight;
  final double roundedContainerHeight;
  final team;
  DetailSilverDelegate(
      this.expandedHeight, this.roundedContainerHeight, this.team);

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    // TODO: implement build
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarBrightness: Brightness.dark,
          statusBarIconBrightness: Brightness.dark),
      child: Hero(
        tag: team.idTeam,
        child: Stack(
          children: <Widget>[
            Image.network(
              team.strStadiumThumb,
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.cover,
            ),
            SafeArea(
                child: Row(
              children: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.arrow_back,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                )
              ],
            )),
            Positioned(
              top: expandedHeight - roundedContainerHeight - shrinkOffset,
              left: 0,
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: roundedContainerHeight,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  ),
                ),
              ),
            ),
            Positioned(
              top: expandedHeight - 120 - shrinkOffset,
              left: 30,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    team.strStadium,
                    style: TextStyle(
                      fontSize: 30,
                      color: Colors.white,
                    ),
                  ),
                  Text(
                    '$team.strStadiumLocation',
                    style: TextStyle(
                      fontSize: 10,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  double get maxExtent => expandedHeight;

  @override
  double get minExtent => 0;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}
