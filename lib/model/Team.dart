// To parse this JSON data, do
//
//     final team = teamFromJson(jsonString);

import 'dart:convert';

Team teamFromJson(String str) => Team.fromJson(json.decode(str));

String teamToJson(Team data) => json.encode(data.toJson());

class Team {
  Team({
    this.idTeam,
    this.strTeam,
    this.strTeamShort,
    this.strAlternate,
    this.strLeague,
    this.strStadium,
    this.strKeywords,
    this.strStadiumThumb,
    this.strStadiumDescription,
    this.strStadiumLocation,
    this.intStadiumCapacity,
    this.strWebsite,
    this.strFacebook,
    this.strTwitter,
    this.strInstagram,
    this.strDescriptionEn,
    this.strCountry,
    this.strTeamBadge,
    this.strTeamJersey,
    this.strTeamLogo,
    this.strTeamFanart1,
    this.strTeamFanart2,
    this.strTeamFanart3,
    this.strTeamFanart4,
    this.strTeamBanner,
  });

  String idTeam;
  String strTeam;
  String strTeamShort;
  String strAlternate;
  String strLeague;
  String strStadium;
  String strKeywords;
  String strStadiumThumb;
  String strStadiumDescription;
  String strStadiumLocation;
  String intStadiumCapacity;
  String strWebsite;
  String strFacebook;
  String strTwitter;
  String strInstagram;
  String strDescriptionEn;
  String strCountry;
  String strTeamBadge;
  String strTeamJersey;
  String strTeamLogo;
  String strTeamFanart1;
  String strTeamFanart2;
  String strTeamFanart3;
  String strTeamFanart4;
  String strTeamBanner;

  factory Team.fromJson(Map<String, dynamic> json) => Team(
        idTeam: json["idTeam"],
        strTeam: json["strTeam"],
        strTeamShort: json["strTeamShort"],
        strAlternate: json["strAlternate"],
        strLeague: json["strLeague"],
        strStadium: json["strStadium"],
        strKeywords: json["strKeywords"],
        strStadiumThumb: json["strStadiumThumb"],
        strStadiumDescription: json["strStadiumDescription"],
        strStadiumLocation: json["strStadiumLocation"],
        intStadiumCapacity: json["intStadiumCapacity"],
        strWebsite: json["strWebsite"],
        strFacebook: json["strFacebook"],
        strTwitter: json["strTwitter"],
        strInstagram: json["strInstagram"],
        strDescriptionEn: json["strDescriptionEN"],
        strCountry: json["strCountry"],
        strTeamBadge: json["strTeamBadge"],
        strTeamJersey: json["strTeamJersey"],
        strTeamLogo: json["strTeamLogo"],
        strTeamFanart1: json["strTeamFanart1"],
        strTeamFanart2: json["strTeamFanart2"],
        strTeamFanart3: json["strTeamFanart3"],
        strTeamFanart4: json["strTeamFanart4"],
        strTeamBanner: json["strTeamBanner"],
      );

  Map<String, dynamic> toJson() => {
        "idTeam": idTeam,
        "strTeam": strTeam,
        "strTeamShort": strTeamShort,
        "strAlternate": strAlternate,
        "strLeague": strLeague,
        "strStadium": strStadium,
        "strKeywords": strKeywords,
        "strStadiumThumb": strStadiumThumb,
        "strStadiumDescription": strStadiumDescription,
        "strStadiumLocation": strStadiumLocation,
        "intStadiumCapacity": intStadiumCapacity,
        "strWebsite": strWebsite,
        "strFacebook": strFacebook,
        "strTwitter": strTwitter,
        "strInstagram": strInstagram,
        "strDescriptionEN": strDescriptionEn,
        "strCountry": strCountry,
        "strTeamBadge": strTeamBadge,
        "strTeamJersey": strTeamJersey,
        "strTeamLogo": strTeamLogo,
        "strTeamFanart1": strTeamFanart1,
        "strTeamFanart2": strTeamFanart2,
        "strTeamFanart3": strTeamFanart3,
        "strTeamFanart4": strTeamFanart4,
        "strTeamBanner": strTeamBanner,
      };
}
